package com.thomaskuenneth.applicationbmicalculator

/**
 * Created by Horst on 02.12.2017.
 * Abschnitt 64.3
 */


import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.content.Context
import android.content.ContentValues
import java.text.SimpleDateFormat //fürs Datum
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*                  // für Locale.*
import com.thomaskuenneth.applicationbmicalculator.BmiDb
import kotlinx.android.synthetic.main.activity_bmi_list.view.*


class BmiDbHandler(context: Context, name: String?, factory:SQLiteDatabase.CursorFactory?,version:Int):
        SQLiteOpenHelper(context, DATABASE_NAME, factory, DATABASE_VERSION){

    /**
     *  Erzeugt eine Tabellenstruktur
     */
    override fun onCreate(db:SQLiteDatabase){
        val CREATE_BMI_TABLE = ("CREATE TABLE " + TABLE_BMI +
                "(" + COLUMN_ID + " INTEGER PRIMARY KEY," +
                COLUMN_DATE + " DATE," +
                COLUMN_Bmi + " DOUBLE" + ")")
        db.execSQL(CREATE_BMI_TABLE)
    }

    override fun onUpgrade(db:SQLiteDatabase,oldVersion:Int,newVersion:Int){
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BMI)
        onCreate(db)
    }

    /*
    * Tabelle und Struktur
     */
    companion object {
        private val DATABASE_VERSION = 1
        private val DATABASE_NAME =  "BmiDb.db" ////"BmiDb.db"
        val TABLE_BMI = "BMI"
        val COLUMN_ID = "id"
        val COLUMN_DATE = "datum"
        val COLUMN_Bmi = "bmi"
    }

    /*
    *  Hinzufügen eines Datensatzes
    */
    fun addBmi(bmidb: BmiDb) {
        val values = ContentValues()
        values.put(COLUMN_DATE, bmidb.datumInDays2000)
        values.put(COLUMN_Bmi, bmidb.bmi)
        val db = this.writableDatabase
        db.insert(TABLE_BMI, null, values)
        db.close()
    }

    /*
    * Selektion von Datensätzen - IN WORK
    */
    fun selectBmi(bmi:Double): BmiDb? {
        val query =  "SELECT * FROM $TABLE_BMI WHERE $COLUMN_Bmi= \"$bmi\""
        val db = this.writableDatabase
        val cursor = db.rawQuery(query, null)
        var result: BmiDb? = null
        if (cursor.moveToFirst()) {
            cursor.moveToFirst()
            val id = Integer.parseInt(cursor.getString(0))
            //val date =  cursor.getString(1)
            val date =  cursor.getDouble(1)
            val bmi = cursor.getDouble(2)
            result = BmiDb(id, date, bmi)
            cursor.close()
        }
        db.close()
        return result
    }


    /*
    * Findet den Datensatz mit der angegebenen ID-Nummer
    */
    fun findRecord(findId: Int): BmiDb{
        val query =  "SELECT * FROM $TABLE_BMI WHERE $COLUMN_ID= \"$findId\""
        val db = this.writableDatabase
        val cursor = db.rawQuery(query, null)
        var record: BmiDb = BmiDb(-1, 0.0, 0.0)
        if(cursor.moveToFirst()){
            cursor.moveToFirst()
            var id = Integer.parseInt(cursor.getString(0))
            var date =  cursor.getDouble(1)
            var bmi = cursor.getDouble(2)
            record = BmiDb(id, date, bmi)
            cursor.close()
        }
        db.close()
        return record
    }

    /*
    * Liefert die letzte Datensatznummer
     */
    fun lastRecordId():Int{

        //val query= "SELECT COUNT(aggregate_expression) FROM $TABLE_BMI"
        val query =  "SELECT * FROM $TABLE_BMI"
        val db = this.writableDatabase
        val cursor=db.rawQuery(query,null)
        cursor.moveToLast()
        val lastId=Integer.parseInt(cursor.getString(0))
        db.close()
        return lastId
    }

    /*
    *  Löschen des gewählten BMI-Datensatzes
    */
    fun deleteBmi(Bmi: Double):Boolean{
        var result = false
        val query =  "SELECT * FROM $TABLE_BMI WHERE $COLUMN_Bmi = \"$Bmi\" "
        val db = this.writableDatabase
        val cursor = db.rawQuery(query, null)
        if (cursor.moveToFirst()){
            val id = Integer.parseInt(cursor.getString(0))
            db.delete(TABLE_BMI, COLUMN_ID + " = ?", arrayOf(id.toString()))
            cursor.close()
            result = true
        }
        db.close()
        return result
    }

    /*
    * Löschen des gewählten Datensatzes
    */
    fun deleteRecord(Id: Int):Boolean{
        var result = false
        val query =  "SELECT * FROM $TABLE_BMI WHERE $COLUMN_ID = \"$Id\" "
        val db = this.writableDatabase
        val cursor = db.rawQuery(query, null)
        if (cursor.moveToFirst()){
            val id = Integer.parseInt(cursor.getString(0))
            db.delete(TABLE_BMI, COLUMN_ID + " = ?", arrayOf(id.toString()))
            cursor.close()
            result = true
        }
        db.close()
        return result
    }

    /*
    * Löschen aller Datensatzes
    */
    fun deleteAllRecords():Boolean{
        var result = false
        val idStart=1
        val query =  "SELECT * FROM $TABLE_BMI WHERE $COLUMN_ID > \"$idStart\" "
        val db = this.writableDatabase
        val cursor = db.rawQuery(query, null)
        cursor.moveToFirst()
        val id = Integer.parseInt(cursor.getString(0))
        db.delete(TABLE_BMI, COLUMN_ID + " >= ?", arrayOf(id.toString()))
        cursor.close()
        result=true
        db.close()
        return result
    }


}  //ende der Klasse