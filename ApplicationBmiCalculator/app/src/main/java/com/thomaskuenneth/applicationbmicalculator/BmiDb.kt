package com.thomaskuenneth.applicationbmicalculator

import android.provider.ContactsContract
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*

/**
 * Created by Jonas on 02.12.2017.
 * Datenstruktur zur Kommunikation mit der Datenbank
 */
class BmiDb {

    val dateformat = SimpleDateFormat("HH:mm:ss MM/dd/yyyy",Locale.US)

    // ATTRIBUTE: Datenstruktur eines Datensatzes
    var id: Int = 0
    //var datum: String        //(01:01:01 09/13/2017)      //SimpleDateFormat("dd.MM.yyyy", Locale.GERMAN)
    var datumInDays2000: Double
    var bmi: Double = 0.0



    // KONSTRUKTOREN
    /**
     * Setzen eines kompletten Datensatzes
     */
    constructor(id: Int, datum: Double, bmi: Double) {
        this.id = id
        this.datumInDays2000=datum
        this.bmi=bmi
    }

    /**
     * id soll von der Datenbank automatisch gesetzt werden
     */
    constructor(datum: Double, bmi: Double) {
        this.datumInDays2000=datum
        this.bmi=bmi
    }

    constructor() {
        val convert = JHconvert()
        val dateformat: SimpleDateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.GERMAN)
        this.datumInDays2000=convert.DateToDays(Date())                         //dateformat.format(Date())
        this.bmi=20.11
    }


    // GETer und SETer sind auch möglich
    val getId:Int
        get()=this.id


    val getBmi:Double
        get()=this.bmi


}