package com.thomaskuenneth.applicationbmicalculator

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_seek_bar_button.*
import android.widget.SeekBar
import android.widget.Button



/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [SeekBarButtonFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 */
class SeekBarButtonFragment : Fragment(), SeekBar.OnSeekBarChangeListener {

    var seekvalue=12

    private var mListener: OnFragmentInteractionListener? = null //TollbarListener ist hier mListener

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view=inflater?.inflate(R.layout.fragment_seek_bar_button, container, false)
        val seekBar: SeekBar? = view?.findViewById(R.id.seekBar_inFragment)
        val button:Button?    = view?.findViewById(R.id.button_inFragment)
        val button8:Button?   = view?.findViewById(R.id.button_inFragment_s8)
        val button10:Button?   = view?.findViewById(R.id.button_inFragment_s10)
        val button14:Button?   = view?.findViewById(R.id.button_inFragment_s14)

        seekBar?.setOnSeekBarChangeListener(this)  //j
        button8?.setOnClickListener{v: View->buttonClicked8(v)}
        button10?.setOnClickListener{v: View->buttonClicked10(v)}
        button14?.setOnClickListener{v: View->buttonClicked14(v)}
        button?.setOnClickListener{v: View->buttonClicked(v)} //j
        return view
    }

    private fun buttonClicked8(view: View){
        mListener?.onButtonClick(8)
    }
    private fun buttonClicked10(view: View){
        mListener?.onButtonClick(10)
    }
    private fun buttonClicked14(view: View){
        mListener?.onButtonClick(14)
    }
    private fun buttonClicked(view: View){
        mListener?.onButtonClick(seekvalue)
    }

    override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean){

        seekvalue=seekBar.progress
    }


    //--- perhaps ---

    override fun onStartTrackingTouch(seekBar: SeekBar?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    //---------------

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(uri)

        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
        fun onButtonClick(position:Int)
    }


}// Required empty public constructor
