package com.thomaskuenneth.applicationbmicalculator

/**
 *  Quellen: mit Hilfe rudimenteren Beispiele
 *  Inhalt: https://coding180.com/kotlin-for-android/
 *   Grafikschrift: https://coding180.com/kotlin-android/draw-text-with-external-sources/
 *   Grafik:  https://coding180.com/kotlin-android/draw-draw-background-and-draw-lines/
 *   (Schriften unter: https://www.creamundo.com/es/search?needle=Acqu)
 *   Schriften+: https://coding180.com/kotlin-android/draw-text/
 *   https://coding180.com/kotlin-android/draw-text/
 *   https://github.com/otfried/cs109-kotlin/blob/master/mini-apps/drawing.kt
 */


import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
//++
import kotlinx.android.synthetic.main.activity_bmi_chart.*
import android.graphics.*
import android.graphics.Paint

import android.view.View
import android.graphics.Canvas

import android.content.Context
import android.graphics.Typeface

import java.util.*

import java.sql.Date
import kotlin.collections.ArrayList

//Attribut mit gültigen, nicht gelöschten BMI-Werten. Der Datenbank entnommen
val bmi= ArrayList<BmiDb>()

class BmiChartActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bmi_chart)
        //my functions

       // val chart = R.id.bmiChart //findViewById(R.id.bmiChart) as android.support.constraint.ConstraintLayout
        val canvas: myCanvas= myCanvas(this)  //Das ist eine Klasse zum Zeichnen
        bmiChart.addView(canvas)
        copyBmiDbToArray()
        //- canvas.testdatenUebernehmen()  //Nur für Test, keine Funktionalität der Anwendung
    } //end of create

    /**
     * Übernehme Datenbankelemente in ein Feld
     */
    fun copyBmiDbToArray():ArrayList<BmiDb> {
        bmi.clear()  //Daten löschen und neu zusammenstellen
// xxxxxxxxxxxx TESTDATEN einfügen xxxxxxxxxxxxxx
        val convert = JHconvert()
        //Test bmi.add(BmiDb(0,convert.DateToDays("05.01.2018"), 20.0) )
        //TEST bmi.add(BmiDb(1,convert.DateToDays("10.01.2018"), 19.8) )
        //TEST bmi.add(BmiDb(2,convert.DateToDays("15.01.2018"), 20.1) )

// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        val bmiDbHandler = BmiDbHandler(this, null, null, 1)
        var nr = 0          //Anzahl der Datensätze
        var lfdNr = 0       //ANzahl der gültigen, nicht gelöschten Datensätze
        var lastId = bmiDbHandler.lastRecordId()

        do {

            val BmiDb= bmiDbHandler.findRecord(nr)
            if (BmiDb.id != -1) {  //Ausgabe nur, wenn es sich um einen nicht gelöschten Wert handelt
                bmi.add(BmiDb)
                lfdNr++
            }
            nr++  //
        } while (BmiDb.id < lastId) //-1 steht für den undefinierten Datensatz, entspricht den Anfangsbedingungen : lastId
        //NUR FÜR TEST
        return  bmi
    } //// ende: copyBmiDbToArray
} //class BmiChartActivity
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class myCanvas(context: Context):View(context){

    /**
     * Das ist ein Testfall
     */
    override fun onDraw(canvas:Canvas){

         Diagrammvorlage(canvas, "Zeit, Datum", "BMI", 170f, 200f, 70.0f)
         SerieBmi(canvas)
    }


    /**
     * Erneuert das Raster
     */
    fun raster( canvas:Canvas, RandLinks:Float, RandRechts:Float, RandOben:Float){
        //Raster
        val diagrammLinks=RandLinks
        val diagrammRechts=width-RandRechts
        val diagrammOben=RandOben
        val diagrammUnten=RandOben+diagrammRechts-diagrammLinks // Damit ist das Diagramm quadratisch
        val xRasterung=4
        val yRasterung=5
        val paintLine = Paint()
        paintLine.setARGB(255, 0, 0, 50) //Rasterfarbe
        paintLine.strokeWidth = 2f   //Rasterlinienstärke
        val dx=(width-RandRechts-RandLinks)/xRasterung //Abstand zwischen den Teilungsslinien bei 5 Linien
        for (x in 0 .. xRasterung){
            canvas.drawLine(diagrammLinks+(dx*x), diagrammOben, diagrammLinks+(dx*x), diagrammUnten, paintLine)
        }
        val dy=(diagrammUnten-diagrammOben)/yRasterung
        for (y in 0 .. yRasterung){
            canvas.drawLine(diagrammLinks, diagrammOben+dy*y, diagrammRechts,diagrammOben+dy*y , paintLine)
        }
    }

    /**
     * Erzeugt die Diagrammvorlage mit Bezeichnung der Achsen
     */
    fun Diagrammvorlage(canvas:Canvas, Abzisse:String, Ordinate: String, RandLinks:Float, RandRechts:Float, RandOben:Float ):RectF{
        canvas.drawRGB(255, 255, 150) //Hintergrundfarbe -> gelblich
        val width = width
        //val height= height
        val diagrammLinks=RandLinks
        val diagrammRechts=width-RandRechts
        val diagrammOben=RandOben
        val diagrammUnten=RandOben+diagrammRechts-diagrammLinks // Damit ist das Diagramm quadratisch
        //val xRasterung=4
        val yRasterung=5
        val textSize=80f
        val paint = Paint()
        // Rasterfläche
        paint.setARGB(255, 240, 240, 240) //Diagrammgrundfarbe weiß
        paint.textSize = 80f
        canvas.drawRect(diagrammLinks,diagrammOben,diagrammRechts,diagrammUnten,paint)
        //Raster
        raster(canvas, RandLinks, RandRechts, RandOben)
        // Beschriftung der Achsen
        val dy=(diagrammUnten-diagrammOben)/yRasterung
        val paintText = Paint()
        paintText.setARGB(255, 0, 0, 250) //Rasterfarbe
        paintText.textSize = textSize   //Textgröße
        paintText.typeface = Typeface.SANS_SERIF
        //val face = Typeface.createFromAsset (getContext().getAssets (),"Acquaintance.ttf")  //Spezielle Schriftart
        //paint.setTypeface (face)
        canvas.drawText(Abzisse,diagrammLinks+120,diagrammUnten+120,paintText)
        canvas.drawText(Ordinate,diagrammLinks-170,diagrammOben+(dy+textSize)/3,paintText)

        return RectF(diagrammLinks,diagrammOben,diagrammRechts,diagrammUnten)
    }




    @SuppressLint("SetTextI18n")
            /**
     *  Skaliert die Achsenwerte und zeichnet die BMI-Kurve
     */
    fun SerieBmi(canvas:Canvas){
        //  bmiChart.
        val diagramm= Diagrammvorlage(canvas, "Zeit, Datum", "BMI", 170f, 200f, 70.0f)
        if (bmi.lastIndex > 0) {
            fun bmiMinMax(): PointF {
                val minmax = PointF()
                minmax.x = bmi[1].bmi.toFloat()   // Min
                minmax.y = bmi[1].bmi.toFloat()
                    for (i in 0..bmi.lastIndex) {
                    if (bmi[i].bmi < minmax.x) minmax.x = bmi[i].bmi.toFloat()
                    if (bmi[i].bmi > minmax.y) minmax.y = bmi[i].bmi.toFloat()
                }
                return minmax
            }
            //Convertieren des DaysDatum in echte Datumswerte
            val bmiDate = Array(bmi.lastIndex + 1, {  Date() })  //beginnt ggf. mit Index=0
            val days = DoubleArray(bmi.lastIndex + 1)
            val convert = JHconvert()
            for (i in 0..bmi.lastIndex) {
                bmiDate[i] = convert.DaysToDate(bmi[i].datumInDays2000)
                days[i]=bmi[i].datumInDays2000
            }
            // Liefert die Indizes der Sortiefolge
            var indexlist=""
            fun valueSortGetIndex(unsortDays: DoubleArray):MutableList<Int> {
                val sortDaysPerIndex : MutableList<Int> =mutableListOf()

                var minBefore = 0.0

                for (j in 0..unsortDays.lastIndex){
                    var min =unsortDays[j]
                    var minIndex=j
                    for (i in 0..unsortDays.lastIndex)
                    {
                        if (unsortDays[i] < min && unsortDays[i]>minBefore){
                            min=unsortDays[i]
                            minIndex=i
                        }
                    }
                    minBefore=min
                    sortDaysPerIndex.add(minIndex)
                    indexlist += minIndex.toString() +", "
                }
                return sortDaysPerIndex
            }

            var sortDaysPerIndex=valueSortGetIndex(days)

            val skalenDatePositionen = 5
            val dateMin = bmi[0].datumInDays2000
            val dateMax = bmi[bmi.lastIndex].datumInDays2000
            /*
            val dateMin = bmi[sortDaysPerIndex[0]].datumInDays2000
            val letzterIndex=sortDaysPerIndex.lastIndex-1
            indexlist += "("+letzterIndex+")"
            val dateMax = bmi[sortDaysPerIndex[letzterIndex]].datumInDays2000
            */
            val bmiMin = bmiMinMax().x
            val bmiMax = bmiMinMax().y
            // Umrechnung der Werte in Pixel
            fun daysToX(days: Double): Float {
                //return diagramm.left + ((days - convert.DateToDays(dateMin)) * (diagramm.right - diagramm.left) / (convert.DateToDays(dateMax) - convert.DateToDays(dateMin))).toFloat()
                return diagramm.left + ((days - dateMin) * (diagramm.right - diagramm.left) / (dateMax - dateMin)).toFloat()
            }

            fun dateToX(datum: Date): Float {
                return diagramm.left + ((convert.DateToDays(datum) - dateMin) * (diagramm.right - diagramm.left) / (dateMax - dateMin)).toFloat()
            }

            fun bmiToY(bmi: Double): Float {
                return diagramm.bottom - ((bmi - bmiMin) * (diagramm.bottom - diagramm.top) / (bmiMax - bmiMin)).toFloat()
            }

            // sollte alten Zustand löschen
            Diagrammvorlage(canvas, "Zeit, Datum", "BMI", 170f, 200f, 70.0f)

            // Zeichnen der Kurve
            val paintKurve = Paint()
            paintKurve.setARGB(255, 255, 0, 50)
            paintKurve.strokeWidth = 5f   //Rasterlinienstärke
            canvas.drawPoint(daysToX(bmi[0].datumInDays2000), bmiToY(bmi[0].bmi), paintKurve)
            for (i in 0..bmi.lastIndex - 1)
            //for (i in (sortDaysPerIndex))
            {
                // zusaetzliche Intervallkontrolle
                if(bmi[i].datumInDays2000>= dateMin && bmi[i].datumInDays2000<= dateMax && bmi[i+1].datumInDays2000>= dateMin && bmi[i+1].datumInDays2000<= dateMax){
                    //canvas.drawLine(daysToX(bmiDaysAfter2000[i]), bmiToY(bmi[i].bmi), daysToX(bmiDaysAfter2000[i + 1]), bmiToY(bmi[i + 1].bmi), paintKurve)
                    canvas.drawLine(daysToX(bmi[i].datumInDays2000), bmiToY(bmi[i].bmi), daysToX(bmi[i+1].datumInDays2000), bmiToY(bmi[i + 1].bmi), paintKurve)
                }
            }

            // Beschriftung der Masszahlen --------
            val paintText = Paint()
            paintText.setARGB(255, 0, 50, 250) //Rasterfarbe
            paintText.textSize = 45f   //Textgröße
            paintText.typeface = Typeface.SANS_SERIF
            val skalenPositionen = 6

            for (i in 0..skalenPositionen - 1) {
                val bmiMasszahlen = bmiMin + (bmiMax - bmiMin) / (skalenPositionen - 1) * (i)
                val bmiMasszahlenPos = (diagramm.bottom + (diagramm.top - diagramm.bottom) / (skalenPositionen - 1) * (i))
                canvas.drawText("%5.2f".format(bmiMasszahlen).toString(), diagramm.left - 100, bmiMasszahlenPos, paintText)
            }

            paintText.textSize = 40f   //Textgröße
            val dateDifferenz = (dateMax - dateMin) / (skalenDatePositionen - 1)
            for (i in 0..skalenDatePositionen - 1) {
                val dateMasszahlen = (dateMin) + dateDifferenz * i
                val dateMasszahlPos = (diagramm.left + (diagramm.right - diagramm.left) / (skalenDatePositionen - 1) * i)
                canvas.drawText(convert.DaysToStringDateddMM(dateMasszahlen), dateMasszahlPos - 70, diagramm.bottom + 50f, paintText)
            }

            // + canvas.drawText("%s".format(indexlist).toString(), diagramm.left - 100, diagramm.bottom + 300f, paintText)

        }
    }
}