package com.thomaskuenneth.applicationbmicalculator

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
// + hinzugefügt
import android.content.Intent
import kotlinx.android.synthetic.main.activity_bmi_result.*
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import android.view.View
import java.sql.SQLDataException
import java.sql.SQLException
//import java.coroutines  ??

class BmiResultActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bmi_result)

        getAnrede()
        showBmiResult()
        findBMICategory()
        showBmiList()
        showBmiChart()
    }
    /**
     * Zeigt den BMI-Wert
     */
    fun showBmiResult(){
         var decFormat=DecimalFormat("#.#")
         var formmattedBmi=decFormat.format(intent.extras.getDouble("BmiResult"))
        textViewBMI.text = formmattedBmi.toString()
     }
    /**
     * Odnet den Wert einer Kategorie zu
     */
    fun findBMICategory(){

        var resultBMI = intent.extras.getDouble("BmiResult")

        var categoryOfBMI = R.string.Normal
        if(resultBMI < 15){
            categoryOfBMI = R.string.verySeverelyUnderweight//"Very Severely Underweight"
        }
        if(resultBMI > 15 && resultBMI <= 16){
            categoryOfBMI = R.string.severelyUnderweight//"Severely Underweight"
        }
        if(resultBMI > 16 && resultBMI <= 18.5){
            categoryOfBMI = R.string.Underweight//"Underweight"
        }
        if(resultBMI > 18.5 && resultBMI <= 25){
            //categoryOfBMI = "Normal (Healthy Weight)"
            categoryOfBMI =R.string.Normal

        }
        if(resultBMI in 25..30){
            categoryOfBMI = R.string.Overweight//"Overweight"
        }
        if(resultBMI in 30..35){
            categoryOfBMI =R.string.moderatelyObese// "Moderately Obese"
        }
        if(resultBMI in 35..40){
            categoryOfBMI = R.string.severelyObese//"Severely Obese"
        }
        if(resultBMI >= 40){
            categoryOfBMI =R.string.verySeverelyObese// "Very Severely Obese"

        }
        // Hinweis: Folgender Ausdruck liefert die Übersetzung: resources.getString(R.string.Normal)
        textViewBmiCategory.text = resources.getString(categoryOfBMI)
    }
    @SuppressLint("SetTextI18n")
            /**
     * Setzt die Anrede zusammen und gibt diese aus
     */
    fun getAnrede(){
        val dateformat: SimpleDateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.GERMAN)
        //Schnittstelle from
        var username= intent.extras.getString("Username")
        var hallo=resources.getString(R.string.hallo)
        var todayOn=resources.getString(R.string.heuteAm)
        var istDeinBodaymassindex=resources.getString(R.string.isYourBodyMassIndex)
        textViewAnrede.text = hallo+" " + username +"!\n"+todayOn +" "+ dateformat.format(Date()) +" "+ istDeinBodaymassindex
    }
    /*
     * Speichert einen Datensatz bzw. hängt diesen an
     */
    fun saveBmi(view: View){
        //buttonSave.setOnClickListener {
        val bmiDbHandler= BmiDbHandler(this,null,null,1)
        //val dateformat: SimpleDateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.GERMAN)
        //val datum= dateformat.format(Date())//Date().toString()
        val convert = JHconvert()
        //val datum= convert.DateToDays(Date())//Date().toString()
        val bmi= intent.extras.getDouble("BmiResult")
        val datum= intent.extras.getDouble("tage2000")
        val bmidb=BmiDb(datum,bmi)
        bmiDbHandler.addBmi(bmidb)
        // Farbe für Ausgabe als Hinweis ändern - geht so nicht
        //textViewBMI.setTextColor(4149685)
        //showBmiResult()
        //}
    }

    @SuppressLint("SetTextI18n")
    fun getRecords():Int{
        try {
            val bmiDbHandler= BmiDbHandler(this,null,null,1)
            textView_Hinweis.text=resources.getString(R.string.records)+bmiDbHandler.lastRecordId().toString()
            return bmiDbHandler.lastRecordId()
        }
        catch (e: SQLDataException){
            textView_Hinweis.text=resources.getString(R.string.keineDaten)
            return 1
        }
    }

    /*
     * Ruft die Seite zur Listenausgabe
     * Achtung: Datei AndroidManifest.xml auch anpassen
     */
    fun showBmiList(){
        buttonBmiListe.setOnClickListener {
            if (getRecords()>=1) {
                val intentList=Intent("com.thomaskuenneth.applicationbmicalculator.BmiListActivity") //"com.thomaskuenneth.applicationbmicalculator.BmiListActivity")
                startActivity(intentList)
            }


        }
    }


    /*
 * Ruft die Seite zur Listenausgabe
 * Achtung: Datei AndroidManifest.xml auch anpassen
 */
    fun showBmiChart() {
        buttonBmiChart.setOnClickListener {
            if (getRecords() > 4) {
                val intentChart = Intent("com.thomaskuenneth.applicationbmicalculator.BmiChartActivity")
                startActivity(intentChart)
            } else {
                textView_Hinweis.text = resources.getString(R.string.DatensaetzeZuGering)
            }
        }
    }
}//endofclass


/*
* OLD METHODS
/*
* Beendet Anwendung
 */
fun setExitListener() {
    buttonEnde.setOnClickListener {
        this.finishActivity(0)
    }
}
/*
* Geht zum Eingabelayout und wiederholt die Berechnung
* Achtung: Datei Manifest muss auch angepasst werden
 */
fun setCheckAgainListener() {
    buttonWiederhole.setOnClickListener {
        // Schnittstelle from
        val intent = Intent("com.thomaskuenneth.applicationbmicalculator.MainActivity")
        startActivity(intent)
    }
}
*/