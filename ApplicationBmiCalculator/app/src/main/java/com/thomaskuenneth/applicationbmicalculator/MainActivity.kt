package com.thomaskuenneth.applicationbmicalculator

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.os.Build
import android.support.annotation.RequiresApi
import kotlinx.android.synthetic.main.activity_main.*
import android.webkit.WebView
import android.webkit.WebViewClient
import android.webkit.WebResourceRequest
import java.lang.Object
import java.util.*

class MainActivity : AppCompatActivity() {

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //systeminfos
        var name= Context.USER_SERVICE

        editTextUsername.setText(name)
        val convert = JHconvert()
        editText_Date.setText(convert.DateToString(Date()))
        //my function
        setOnClickListenerForButton()
        showOpenData()
    }
    //FUNKTIONEN
    /**
     * Holt die Eingabedaten und berechnet den BMI-Wert
     */
    fun calculateBmi():Double{

        var gewicht: Double=(editTextGewicht.text.toString().toDouble())
        var groesse: Double=(editTextGroesse.text.toString().toDouble())
        var BmiResult= (((gewicht/groesse)/groesse))
        return  BmiResult
    }
    /**
     *  Schnittstelle
     *  Ruft calculateBmi auf und wechselt die Activity -->"BmiResultActivity"
     */
    fun setOnClickListenerForButton(){
        btnCalculateBMI.setOnClickListener {
            val convert = JHconvert()
            val days=convert.DateToDays(editText_Date.text.toString())

            val intent=Intent("com.thomaskuenneth.applicationbmicalculator.BmiResultActivity")
            intent.putExtra("BmiResult", calculateBmi())
            intent.putExtra("Username", editTextUsername.text.toString())
            intent.putExtra("tage2000",days)
            startActivity(intent)
        }
    }

    fun showOpenData(){
        button_openData.setOnClickListener{
            val intent = Intent("com.thomaskuenneth.applicationbmicalculator.openDataActivity") 
            //val intent = Intent("com.thomaskuenneth.applicationbmicalculator.BmiBerlinOpenActivity")

            startActivity(intent)

        }

    }



} //endofclass
