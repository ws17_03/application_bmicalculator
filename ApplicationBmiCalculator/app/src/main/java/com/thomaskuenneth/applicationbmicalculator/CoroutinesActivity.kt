package com.thomaskuenneth.applicationbmicalculator

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.media.MediaBrowserServiceCompat
import kotlinx.android.synthetic.main.activity_coroutines.*
import kotlin.coroutines.*
import kotlinx.coroutines.*
import kotlinx.coroutines.experimental.runBlocking
import org.jetbrains.anko.UI
import org.jetbrains.anko.coroutines.experimental.asReference
import kotlin.coroutines.experimental.*
import org.jetbrains.anko.coroutines.*
import org.jetbrains.anko.coroutines.experimental.bg
import kotlin.system.measureTimeMillis


class CoroutinesActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_coroutines)


        fun <T> async(block: suspend () -> T){}

        //
        val time= measureTimeMillis {
            //async(start = CoroutineStart.LAZY) { mySuspending() }
            runBlocking {
                //val result=async {  mySuspending() }
                val result=mySuspending()
                textView_coroutines.setText("async: "+result)
            }
        }



        async(){
            val result =mySuspending()
        }


    }

    suspend fun mySuspending():String{
        val i=5
        //TODO()




        return "mySuspending - "+i.toString()
    }


















}
