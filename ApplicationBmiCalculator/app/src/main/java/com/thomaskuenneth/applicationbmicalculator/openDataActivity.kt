package com.thomaskuenneth.applicationbmicalculator

import android.os.Bundle
import kotlinx.android.synthetic.main.activity_open_data.*
import kotlinx.coroutines.experimental.runBlocking
import kotlin.system.measureTimeMillis

import android.support.v4.app.FragmentActivity
import android.util.Log
import java.net.ConnectException
import java.net.URL
import java.io.File
import java.io.InputStream
import kotlin.io.inputStream
import kotlin.io.readText
import android.net.Uri
import org.jetbrains.anko.longToast
import java.util.concurrent.Executors


private var bmiTest="Tabelle 4.8-2:;;;;;;;;;Indikator 4.8;;;Metadaten\n" +
        "\"Body-Mass-Index (BMI) der erwachsenen Bev”lkerung in Berlin 2009\n" +
        "nach Geschlecht und Alter\";;;;;;;;;;;;\n" +
        ";;;;;;;;;;;;\n" +
        ";;;;;;;;;;;;\n" +
        ";;;;;;;;;;;;\n" +
        "\"Geschlecht /\n" +
        "Alter in Jahren \";\"Bev”lkerung \u0081ber 18 Jahren mit\n" +
        "Angaben zu Gr”áe und Gewicht\";;Davon mit einem Body-Mass-Index (BMI)1) von ...;;;;;;;;;\n" +
        ";;;unter 18,5;;18,5 - 24,9;;25 - 29,9;;30 und mehr;;;\n" +
        ";in 1.000;;in 1.000;in %;in 1.000;in %;in 1.000;in %;in 1.000;in %;;\n" +
        ";;;;;;;;;;;;\n" +
        "weiblich;;;;;;;;;;;;\n" +
        ";;;;;;;;;;;;\n" +
        "unter 25;131,7;;13,8;10,5;103,8;78,8;11,1;8,4;/;/;;\n" +
        "25 - 34;184,2;;14,1;7,6;135,3;73,4;25,9;14,1;8,9;4,8;;\n" +
        "35 - 44;192,9;;7,7;4,0;129,0;66,9;37,7;19,5;18,5;9,6;;\n" +
        "45 - 54;184,3;;5,5;3,0;104,8;56,9;52,4;28,4;21,8;11,8;;\n" +
        "55 - 64;159,8;;/;/;75,1;47,0;54,7;34,2;28,5;17,8;;\n" +
        ";;;;;;;;;;;;\n" +
        "25 - 64;721,1;;29,5;4,1;444,2;61,6;170,7;23,7;77,7;10,8;;\n" +
        ";;;;;;;;;;;;\n" +
        "65 u. mehr;320,2;;7,7;2,4;138,7;43,3;118,8;37,1;55,9;17,4;;\n" +
        ";;;;;;;;;;;;\n" +
        "zusammen;1.173,0;;51,1;4,3;686,7;58,6;300,6;25,6;136,7;11,7;;\n" +
        ";;;;;;;;;;;;\n" +
        "m„nnlich;;;;;;;;;;;;\n" +
        ";;;;;;;;;;;;\n" +
        "unter 25;131,6;;6,9;5,2;93,3;70,9;26,8;20,4;/;/;;\n" +
        "25 - 34;194,9;;/;/;119,4;61,3;59,2;30,4;14,1;7,2;;\n" +
        "35 - 44;201,2;;/;/;94,6;47,0;82,3;40,9;23,1;11,5;;\n" +
        "45 - 54;190,0;;/;/;78,3;41,2;85,3;44,9;25,2;13,3;;\n" +
        "55 - 64;155,5;;/;/;50,2;32,3;74,0;47,6;30,6;19,6;;\n" +
        ";;;;;;;;;;;;\n" +
        "25 - 64;741,6;;5,3;0,7;342,5;46,2;300,8;40,6;93,0;12,5;;\n" +
        ";;;;;;;;;;;;\n" +
        "65 u. mehr;237,7;;/;/;76,2;32,1;123,4;51,9;36,9;15,5;;\n" +
        ";;;;;;;;;;;;\n" +
        "zusammen;1.110,8;;13,6;1,2;511,9;46,1;451,0;40,6;134,5;12,1;;\n" +
        ";;;;;;;;;;;;\n" +
        "insgesamt;;;;;;;;;;;;\n" +
        ";;;;;;;;;;;;\n" +
        "unter 25;263,3;;20,7;7,9;197,1;74,8;37,9;14,4;7,8;2,9;;\n" +
        "25 - 34;379,1;;16,3;4,3;254,7;67,2;85,1;22,4;23,0;6,1;;\n" +
        "35 - 44;394,0;;8,8;2,2;223,7;56,8;120,0;30,5;41,7;10,6;;\n" +
        "45 - 54;374,3;;6,7;1,8;183,1;48,9;137,7;36,8;47,1;12,6;;\n" +
        "55 - 64;315,3;;/;/;125,2;39,7;128,7;40,8;59,0;18,7;;\n" +
        ";;;;;;;;;;;;\n" +
        "25 - 64;1.462,7;;34,8;2,4;786,6;53,8;471,5;32,2;170,8;11,7;;\n" +
        ";;;;;;;;;;;;\n" +
        "65 u. mehr;557,9;;9,2;1,6;215,0;38,5;242,2;43,4;92,7;16,6;;\n" +
        ";;;;;;;;;;;;\n" +
        "insgesamt;2.283,8;;64,7;2,8;1.198,7;52,5;751,5;32,9;271,3;11,9;;\n" +
        ";;;;;;;;;;;;\n" +
        ";;;;;;;;;;;;\n" +
        "/ Zahlenwert nicht sicher genug.;;;;;;;;;;;;\n" +
        "1) Der BMI ist ein Maá zur Beurteilung des K”rpergewichts. Er berechnet sich nach der Formel BMI = K”rpergewicht [kg] / K”rpergr”áe [m]2.;;;;;;;;;;;;"

class openDataActivity : FragmentActivity(), SeekBarButtonFragment.OnFragmentInteractionListener  {  //AppCompatActivity(),

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_open_data)



        //http://www.gsi-berlin.info/gsi_suchen.asp?seite=2&cbfest=Kategorie,Bereich,Thema,Unterthema&kategorie=Gesundheitsdaten&bereich=Gesundheitsrelevante Verhaltensweisen&thema=Ern%e4hrung&unterthema=K%f6rpergewicht
        val urlBmi="http://www.gsi-berlin.info/redirectA.asp?filename=TG0100048124201300.xls"
        val urlName="http://www.berlin.de/daten/liste-der-vornamen-2016/charlottenburg-wilmersdorf.csv" //zum Testen

       measureTimeMillis {
            //async(start = CoroutineStart.LAZY) { suspendingBuildDataPool() }
           runBlocking(block={
               this@openDataActivity.susBuildData(url=urlBmi)
           })
        }

        fun <T> async(block: suspend () -> T) {
            val executor=Executors.newScheduledThreadPool(4)
            async {
                this.susBuildData(url=urlBmi.toString())
            }
        }
    }
    // implement j. +for fragment  +++++++++++++++
    override fun onFragmentInteraction(uri: Uri) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
    //+for fragment
    override fun onButtonClick(position:Int){
        textView_BerlinBmi.textSize = position.toFloat()
    }
    //+++++++++++++++++++++++++++++++++++++++
    /**
     *  Liest Daten, wertet diese aus und gibt sie aus
     */
    private suspend fun susBuildData(url: String)
    {
        var response= ""//bmiTest.toString()

        //test: val response=URL(url).readBytes()  //TODO  //charset("Charsets.UTF_8")

        var TAG="openDataActivity"
        try{
            Log.i(TAG, "Start:" + url)
            response=URL(url).readText(charset("Charsets.UTF_8"))  //TODO  //charset("Charsets.UTF_8")
            File("BmiBerlinCsv12.xls").writeText(response, charset("Charsets.UTF_8"))
        } catch (ce: ConnectException){  //MalformedURLException
            val inputStream: InputStream=File("BmiBerlinCsv12.xls").inputStream()
            response = inputStream.bufferedReader().use { it.readText() }
            response += "("+ce.toString()+")"
        } catch(e: Exception){
            response = "(e: "+e.toString()+")\n" + bmiTest
        }
        val responseString=response.lines().toMutableList()
        responseString.removeAt(0)  // entfern At
        var dataList: MutableList<String>
        var listKlassen: MutableList<String>   = mutableListOf("unter 18,5:", " 18,5 - 25,9:", " 25.0 - 29,9:","        ab 30:")
        var listWeiblich: MutableList<String>  = mutableListOf("","","","")
        var listMaennlich: MutableList<String> = mutableListOf("-","","","")
        var listGesamt: MutableList<String>    = mutableListOf("","","","")

        var merkerWeiblich=false
        var merkerMaennlich=false
        var merkerInsgesamt=false
        // Auswerten der Daten - Gesucht werden Übersichtsdaten
        // Das ist etwas aufwändig aufgrund der komplexen Struktur der Primärtabelle
        for (date in responseString)
        {
            dataList=date.split(";").toMutableList()
            if (dataList[0]=="weiblich"){merkerWeiblich=true}
            if (dataList[0]=="m„nnlich"){merkerMaennlich=true}
            if (dataList[0]=="insgesamt"){merkerInsgesamt=true}

            if (merkerWeiblich  and (dataList[0]=="zusammen") and (dataList.size>10)){
                listWeiblich[0]=dataList[4]
                listWeiblich[1]=dataList[6]
                listWeiblich[2]=dataList[8]
                listWeiblich[3]=dataList[10]
                merkerWeiblich=false //erledigt
            }

            if (merkerMaennlich  and (dataList[0]=="zusammen") and (dataList.size>10)){
                listMaennlich[0]=dataList[4]
                listMaennlich[1]=dataList[6]
                listMaennlich[2]=dataList[8]
                listMaennlich[3]=dataList[10]
                merkerMaennlich=false //erledigt
            }

            if (merkerInsgesamt  and (dataList[0]=="insgesamt") and (dataList.size>10)){
                listGesamt[0]=dataList[4]
                listGesamt[1]=dataList[6]
                listGesamt[2]=dataList[8]
                listGesamt[3]=dataList[10]
                merkerInsgesamt=false //erledigt
            }
        }
        // Ausgabe
        //var ueberschrift=resources.getString(R.string.berlinUberschrift)


       // var longText="Verteilung der BM-Indizes in Berlin \n für alle Altersgruppen\n"
        //longText   +="(Angaben der Verteilungswerte in %) \n\n\n"

         var longText=resources.getString(R.string.berlinUberschrift)+"\n\n"
        longText += String.format("%15s %10s %10s %10s\n\n","BMI-Intervall","weiblich","männlich","gesamt")
        for (i in (0..3)){
            longText += String.format("%-13s %10s %12s %12s\n\n",listKlassen[i],listWeiblich[i],listMaennlich[i], listGesamt[i])

        }
        textView_BerlinBmi.text=longText
        Log.i(TAG, "Show in TextView")


    }





} //class
