/**
 * Convertiert Datum in String-Darstellungen und zurück.
 * Berechnet die Anzahl der Tage bezogen auf das Jahr 2000
 * Created by Jonas H on 25.12.2017.
 */
package com.thomaskuenneth.applicationbmicalculator

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

class JHconvert {
    @SuppressLint("SimpleDateFormat: \"yyyy-MM-dd'T'HH:mm'Z'\"", "SimpleDateFormat")
            /**
     * Datum To String (aus Java importiert)
     */
    fun toISO8601UTC(date: Date): String {
        val tz = TimeZone.getTimeZone("UTC")
        val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'")
        df.timeZone = tz
        return df.format(date)
    }


    @SuppressLint("SimpleDateFormat")
            /**
     * Datum To String (aus Java importiert)
     */
    fun toISO8601UTC(date: Date, format:String): String {
        val tz = TimeZone.getTimeZone("UTC")
        val df = SimpleDateFormat(format)
        df.timeZone = tz
        return df.format(date)
    }
//-----------------------------------------------------------------------------------------
    @SuppressLint("SimpleDateFormat: \"dd.MM.yyyy\" ", "SimpleDateFormat")
            /**
     * String to Datum (aus Java importiert, Änderungen waren erforderlich)
     */
    fun fromISO8601UTC(dateStr: String): Date {
        val tz = TimeZone.getTimeZone("UTC")
        //val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'")
        // WICHTIG: Nehme die passende Schablone
        val df = SimpleDateFormat("dd.MM.yyyy")
        df.timeZone = tz

        try {
            return df.parse(dateStr)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        // Wenn nichts geht
        return fromISO8601UTC("01.01.2000")
    }


    @SuppressLint("SimpleDateFormat")
            /**
     * String to Datum (aus Java importiert, Änderungen waren erforderlich)
     */
    fun fromISO8601UTC(dateStr: String, format:String): Date {
        val tz = TimeZone.getTimeZone("UTC")
        //val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'")
        // WICHTIG: Nehme die passende Schablone
        val df = SimpleDateFormat(format)
        df.timeZone = tz

        try {
            return df.parse(dateStr)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        // Wenn nichts geht
        return fromISO8601UTC("01.01.2000")
    }

//----------------------------------------------------------------------------------------
    fun DateToDays(datum: Date):Double
    {
        val jahre=toISO8601UTC(datum, "yy").toLong()
        val monat=toISO8601UTC(datum, "MM").toLong()
        val tag=toISO8601UTC(datum, "dd").toLong()
       // ungefähr reicht hier
        val days=(jahre * 360 +(monat-1) *30 + tag-1).toDouble()
       return days
    }

    fun DateToString(datum: Date):String{
        val jahre=toISO8601UTC(datum, "yy")
        val monat=toISO8601UTC(datum, "MM")
        val tag=toISO8601UTC(datum, "dd")
        return tag+"."+monat+"."+jahre
    }

    fun DateToDays(dateStr: String ):Double
    {
        var date = fromISO8601UTC(dateStr)
        val jahre=toISO8601UTC(date, "yy").toLong()
        val monat=toISO8601UTC(date, "MM").toLong()
        val tag=toISO8601UTC(date, "dd").toLong()
        // ungefähr reicht hier
        val days=(jahre * 360 +(monat-1) *30 + tag-1).toDouble()
        return days
    }




    fun DaysToDate(days: Double):Date{
        return fromISO8601UTC(DaysToStringDate(days), "yy-MM-dd")
    }

    //--------------------------------------------------------------------------------------
    fun DaysToStringDate(days: Double):String{
        val jahre =  days / 360
        val daysRest= days % 360
        val monate = (daysRest / 30)
        val tage= daysRest % 30  //=monateRest
        val dateString= "%2s".format(jahre.toInt()).toString()+"/"+"%2s".format(monate.toInt()+1).toString()+"/"+"%2s".format(tage.toInt()+1).toString()
        return dateString
    }

    fun DaysToStringDateddMMyyyy(days: Double):String{
        val jahre =  days / 360
        val daysRest= days % 360
        val monate = (daysRest / 30)
        val tage= daysRest % 30  //=monateRest
        //val dateString= "%2s".format(jahre.toInt()).toString()+"/"+"%2s".format(monate.toInt()+1).toString()+"/"+"%2s".format(tage.toInt()+1).toString()
        val dateString="%2s".format(tage.toInt()+1).toString()+ "."+ "%2s".format(monate.toInt()+1).toString() +"."+"%4s".format(jahre.toInt()).toString()
        return dateString
    }

    fun DaysToStringDateddMMyy(days: Double):String{
        val jahre =  days / 360
        val daysRest= days % 360
        val monate = (daysRest / 30)
        val tage= daysRest % 30  //=monateRest
        //val dateString= "%2s".format(jahre.toInt()).toString()+"/"+"%2s".format(monate.toInt()+1).toString()+"/"+"%2s".format(tage.toInt()+1).toString()
        val dateString="%2s".format(tage.toInt()+1).toString()+ "."+ "%2s".format(monate.toInt()+1).toString() +"."+"%2s".format(jahre.toInt()).toString()
        return dateString
    }
    fun DaysToStringDateddMM(days: Double):String{
        val jahre =  days / 360
        val daysRest= days % 360
        val monate = (daysRest / 30)
        val tage= daysRest % 30  //=monateRest
        //val dateString= "%2s".format(jahre.toInt()).toString()+"/"+"%2s".format(monate.toInt()+1).toString()+"/"+"%2s".format(tage.toInt()+1).toString()
        val dateString="%2s".format(tage.toInt()+1).toString()+ "."+ "%2s".format(monate.toInt()+1).toString()
        return dateString
    }



}