package com.thomaskuenneth.applicationbmicalculator

//import android.support.v7.app.AppCompatActivity
import android.support.v4.app.FragmentActivity

import android.os.Bundle
import kotlinx.android.synthetic.main.activity_bmi_list.*
import java.text.DecimalFormat
import android.content.Intent
import android.net.Uri
import android.view.KeyEvent
import android.widget.TextView


// + hinzugefügt
import java.text.SimpleDateFormat
import java.util.*
import android.view.View
import com.thomaskuenneth.applicationbmicalculator.BmiDb
import com.thomaskuenneth.applicationbmicalculator.BmiDbHandler
import kotlinx.android.synthetic.main.activity_bmi_result.*

//class BmiListActivity : AppCompatActivity() {
class BmiListActivity : FragmentActivity(), SeekBarButtonFragment.OnFragmentInteractionListener  {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bmi_list)
        //my methods
        showBmiDbList()
        deleteNextRecords()
        deleteRecord()
    }

    // implement j. +++++++++++++++++++++++
    override fun onFragmentInteraction(uri: Uri) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
    override fun onButtonClick(position:Int){
        textViewBmiList.textSize = position.toFloat()
    }
    /**
     * Zeigt den BMI-Wert
     */
    fun showBmiDbList(){
        val bmiDbHandler = BmiDbHandler(this, null, null, 1)
        var nr=1
        var lfdNr=1
        var lastId=bmiDbHandler.lastRecordId()
        // var record=" BMI-Liste (letzte Id:" + lastId.toString() +")\n"
        var record=resources.getString(R.string.titelTable) + lastId.toString() +"\n"
       //record+="Lfd.-Nr.        Id       Datum         BMI\n"
        record+= resources.getString(R.string.headerOfTheTable)+"\n"
        val convert = JHconvert()
        do {
            val BmiDb = bmiDbHandler.findRecord(nr)
            val datum=convert.DaysToStringDateddMMyy(BmiDb.datumInDays2000)
           // val bmi="%5.2f".format(BmiDb.bmi).toString()
            if (BmiDb.id != -1) {  //Ausgabe nur, wenn es sich um einen nicht gelöschten Wert handelt
                //record += lfdStrNr + "       "+ id + "      " + datum + "   " + bmi + "\n"
                record +=String.format("%6d %8d %20s %10.2f \n",lfdNr ,BmiDb.id ,datum, BmiDb.bmi)
                lfdNr++
            }
            nr++
        } while (BmiDb.id != lastId) //-1 steht für den undefinierten Datensatz, entspricht den Anfangsbedingungen
        textViewBmiList.text = record
    }

    /**
     * Löscht alle Datensätze
     */
    fun deleteNextRecords() {
        buttonDeleteAll.setOnClickListener {
            val bmiDbHandler = BmiDbHandler(this, null, null, 1)
            val deleted = bmiDbHandler.deleteAllRecords()
          }
    }


    /**
     * Löscht den Datensatz mit der im Eingabefeld angegeben Id
     */
    fun deleteRecord() {
        buttonDeleteRecord.setOnClickListener {
            val bmiDbHandler = BmiDbHandler(this, null, null, 1)
            var id = editTextDeleteRecord.text.toString().toInt()
            bmiDbHandler.deleteRecord(id)
            showBmiDbList()
            //textViewBmiList.setText("gelöscht")
        }
    }
}



