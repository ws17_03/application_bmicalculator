# BmiCalculator

Der BmiCalculator hilft den BMI zu errechnen und ihn zu verbessern.
[https://bitbucket.org/ws17_03/application_bmicalculator)


## Kontaktiere die Entwickler
- Jonas Heinke
- Nils Machate


## Screenshots
![firstscreen](https://www2.pic-upload.de/img/34719104/bmi1.png)
![secondscreen](https://www2.pic-upload.de/img/34719107/bmi2.png)
![tablescreen](https://www2.pic-upload.de/img/34719108/bmi3.png)
 
### External libraries/frameworks/etc.

Projects being used in this app e.g. :

- Android API 23 Platform
- Java 1.8
- OSMBonusPack
- osmdroid-mapsforge:5.5:release@aar
- mapsforge-map-android:0.6.1
- mapsforge-map:0.6.1
- osmdroid-android:5.6.2
- support-v4:25.1.0
- design:25.1.0
- junit:4.12
- mockito-core:2.0.57-beta

##Permissions

- ACCESS_COARSE_LOCATION
- ACCESS_FINE_LOCATION
- ACCESS_WIFI_STATE
- ACCESS_NETWORK_STATE
- INTERNET
- WRITE_EXTERNAL_STORAGE


## Getting Started

Installieren der App:
1. Downloade Android Studio (https://developer.android.com/studio/index.html) 
2. Installiere Android Studio
3. Lade das Projekt (https://bitbucket.org/ws17_03/application_bmicalculator) herunter.
4. �ffne das Projekt �ber Android Studio.
5. Starte das Projekt mit einem Emulator

## Usecase
1. Max f�hlt sich nicht Fit
2. Max wiegt sein Gewicht
3. Max misst wie gro� er ist
4. Max �ffnet den BmiCalculator
5. Max l�sst seinen BMI berechnen
6. Er ist nicht sehr begeistert von seinem BMI und will ihn verbessern.
7. Max macht Sport.
8. Am n�chsten Tag misst Max seinen BMI nochmal
9. Max misst die ganze Woche nach und bekommt dank BMI gezeigt ob er seinen BMI verbessern konnte.

## FAQ

## Testing


## Lizenz
keine